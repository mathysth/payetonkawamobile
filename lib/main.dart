import 'package:flutter/material.dart';

import 'app/home/home.module.dart';

void main() {
  runApp(const HomeModule());
}